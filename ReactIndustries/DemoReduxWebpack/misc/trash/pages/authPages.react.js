
import React from "react"
import {BrowserRouter, withRouter, Route, Redirect} from "react-router-dom"


const fakeAuth = {
  isAuthenticated: false,
  authenticate(callback) {
    this.isAuthenticated = true
    setTimeout(callback, 100) // fake async
  },
  signout(callback) {
    this.isAuthenticated = false
    setTimeout(callback, 100)
  }
}

const AuthComponent = withRouter(({ history }) => (
  fakeAuth.isAuthenticated ? (
    <p>
      Welcome User! <button onClick={() => {
        fakeAuth.signout(() => history.push('/'))
      }}>Sign out</button>
    </p>
  ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
      )
   	 
))

class Login extends React.Component {
  constructor(props){
  	super(props);
  	this.state = {};
  	this.state.redirectToReferrer = false;
  	this.login = this.login.bind(this);
  }

  login(){
    fakeAuth.authenticate(() => {
      this.setState({ redirectToReferrer: true })
    })
  }

  render() {
    console.log("Login:render()")
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    const { redirectToReferrer } = this.state
    
    if (redirectToReferrer) {
      console.log("Login:render():redirectToReferrer->" + from)
      return (
        <Redirect to={from}/>
      )
    }

    return (
      <div>
        <p>You must log in to view the page at {/*from.pathname*/}</p>
        <button onClick={this.login}>Log in</button>
      </div>
    )
  }
}

class AuthPage extends React.Component{

	render1(){
		console.log("AuthPage:render1");
		if(fakeAuth.isAuthenticated){
			return (
				<h2> Hello You are logged in</h2>
			);
		}else{
	      return( 
		      <Redirect to={{
		        pathname: '/login',
		        state: { from: this.props.location }
		      }}/>
	      );
		}
	}

	render(){
		return(
			<BrowserRouter>
				<div>
      				<Route path="/login" component={Login}/>
      				
				</div>
			</BrowserRouter>
		);
	}
}


export{LoginPage}

