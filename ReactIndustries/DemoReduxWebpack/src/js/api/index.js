import 'es6-promise';

function loadModel(id){
  var models = require('./model.json').data;
  return new Promise(sCallback =>{
    setTimeout(() =>{
      sCallback(models[id]);
    }, 2 * 1000); // delay 2 seconds
  }, eCallback=>{
    console.log("api:loadModel->Error");
    return "Error";
  });
}

export default{
  loadModel,
};
