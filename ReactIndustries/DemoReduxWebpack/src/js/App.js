
import React from "react"
//import {Router, browserHistory} from "react-router"
/*import {
  BrowserRouter as Router,
  Switch,
  Route,
  browserHistory,
  Link,
  IndexRoute
} from 'react-router-dom'*/

import ReactReportView from "./views/ReactReport"

import Routes from "./routes";

class App extends React.Component{

	render(){
    //console.log("App.render");
		return (
			<div>
					<Routes/>
			</div>
		);
	}
};

export default App;
