
import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  IndexRoute
} from 'react-router-dom'


import HomePage  from "./pages/Home";
import ReactReportPage from "./pages/ReactReport";
import NotFoundPage from "./pages/NotFound";
import MenuView from "./views/navigation/MenuView";
//import AboutPage  from "./pages/aboutPage.react";
//import ContactPage  from "./pages/contactPage.react";

const Routes = () =>{
  	return(<Router>
      <div>
          <MenuView/>
          <Switch>
              <Route exact path="/" component={ReactReportPage}/>
              <Route exact path="/home" component={HomePage}/>
              <Route component={NotFoundPage} />
          </Switch>
      </div>
    </Router>
  );
};

export default Routes;
