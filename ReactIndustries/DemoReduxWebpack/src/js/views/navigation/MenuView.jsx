import React, { Component } from 'react';
import Link from 'react-router-dom/Link';
import "./HomeMenu.css";

export default class MenuView extends Component {

  render() {
    return (
      <div className='Home'>
          <Link to="/home">
              Home
          </Link>
          <Link to="/">
              React Report
          </Link>
          <Link to='404'>
              404
          </Link>
      </div>
    );
  }
}
