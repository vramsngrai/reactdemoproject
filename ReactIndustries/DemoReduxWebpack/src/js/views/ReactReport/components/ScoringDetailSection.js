
import React from 'react';
import SectionHeader from './SectionHeader';
/*import { Rating } from 'material-ui-rating'*/
//import {MaterialButtonStyles} from './AppStyles'
//const { ContentAddCircle, ContentAddCircleOutline, ContentRemove } = require('material-ui/svg-icons');
var Rating = require('react-rating');


class ScoringDetailSection extends React.Component{
	constructor(props){
		super(props);
		this.keyMetricHeader = ['Key Metrics', 'Average', 'Your Score'];
		this.benefitHeader = ['Benefits (self-reported)', 'Plan', 'Standard'];
	}

	loadData(){
		this.keyMetriData = []
		let metricModels = this.props.model.key_metrics;
		metricModels.map((item, index) =>{
			this.keyMetriData[index] =  { item1: item.name, item2: item.avg, item3: item.score, item4: item.rating};
		});
/*		this.keyMetriData[0] = { item1: 'Metric 1', item2: '2% to 7%', item3: '12%', item4: 5};
		this.keyMetriData[1] = { item1: 'Metric 2', item2: '70% to 80%', item3: '48%', item4: 1.5};
		this.keyMetriData[2] = { item1: 'Metric 3', item2: '3% to 7%', item3: '42%', item4: 5};
		this.keyMetriData[3] = { item1: 'Metric 4', item2: '80%', item3: '72%'};*/
		//this.keyMetriData = this.props.model.key_metrics;

		this.benefitData = []
		let benefitModels = this.props.model.benifits;
		benefitModels.map((item, index)=>{
			this.benefitData[index] = { item1: item.name, item2: item.is_plan, item3: item.is_standard, item4: item.state};
		});
/*		this.benefitData[0] = { item1: 'Benefit 1', item2: 'Yes', item3: 'No', item4: 'Partial'};
		this.benefitData[1] = { item1: 'Benefit 2', item2: 'Yes', item3: 'Yes', item4: 'Full'};
		this.benefitData[2] = { item1: 'Benefit 3', item2: 'N/A', item3: 'N/A', item4: 'N/A'};
*/

	}




	render(){
		this.loadData();
		return (
			<div>
				<SectionHeader title="SCORING DETAILS" fontSize={20} />
				<ScoringTable header = {this.keyMetricHeader} rowData = {this.keyMetriData} type={'metric'}/>
				<ScoringTable header = {this.benefitHeader} rowData = {this.benefitData} type={'benefit'}/>
			</div>
			);
	}
}


class ScoringTable extends React.Component{
/*	constructor(props){
		super(props);
	}*/


	renderHelperGetItem(data){
		if(data === undefined){
			return;
		}
		if(this.props.type === 'metric'){
			return <Rating
					  placeholderRate={data}
					  empty={<img alt='icon' src="src/assets/images/star-empty.png" className="icon ScoringDetailSection-RatingIcon" />}
					  placeholder={<img alt='icon' src="src/assets/images/star-full.png" className="icon ScoringDetailSection-RatingIcon" />}
					  full={<img alt='icon' src="src/assets/images/star-full.png" className="icon ScoringDetailSection-RatingIcon" />}
					  readonly
					/>
	    }else{
	    	return data;
	    }
	}

	render(){
		return (
			<div className="content">
				<table className="ScoringTable-table">
					<thead>
						<tr>
							<th colSpan={4}> {this.props.header[0]} </th>
							<th> {this.props.header[1]} </th>
							<th> {this.props.header[2]} </th>
						</tr>
					</thead>
					<tbody>
						{
							this.props.rowData.map( (row, index) => (
								<tr key={index}>
									<td colSpan={4}> {row['item1']} </td>
									<td> {row['item2']} </td>
									<td> {row['item3']} </td>
									<td> {this.renderHelperGetItem(row['item4'])} </td>
								</tr>
							))

						}

					</tbody>

				</table>
			</div>
		);
	}
}


export default ScoringDetailSection;
