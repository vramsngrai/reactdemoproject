
import React from 'react'
var Rating = require('react-rating');

/*const NOTE_VALUES = {
	overall: 'Your overall score is %s1 in %s2',
	bestArea: 'Your best area for scoring improvement is %s1.',
	bestBenefit: 'Your Benefits raised your score from %s1 to %s2',
	questionAs: 'One or more of your benefits questions has not been answered. Providing this information could increase your overall score.',
	percentTmpWorker: 'The % of temp workers is %s1%'
};
*/

class ComponentHelper{
}

ComponentHelper.getRatingComponent = function(value){
	if(value === undefined){
		return;
	}
	return <Rating
			  placeholderRate={value}
			  empty={<img  alt='icon' src="src/assets/images/star-empty.png" className="icon ScoringDetailSection-RatingIcon" />}
			  placeholder={<img  alt='icon'src="src/assets/images/star-full.png" className="icon ScoringDetailSection-RatingIcon" />}
			  full={<img  alt='icon' src="src/assets/images/star-full.png" className="icon ScoringDetailSection-RatingIcon" />}
			  readonly
			/>
}

ComponentHelper.getNoteValue = function(type, values){
	//var note = NOTE_VALUES[type];
	var result = '';
	switch(type){
		case 'overall':{
			result = <span> Your overall score is {values[0]} in {values[1]}.</span>;
		}
		break;
		case 'bestArea':
			result = <span> Your best area for scoring improvement is {values[0]}.</span>;
		break;
		case 'bestBenefit':
			result = <span> Your Benefits raised your score from
			{ComponentHelper.getRatingComponent(values[0])} to {ComponentHelper.getRatingComponent(values[1])}.</span>;
		break;
		case 'questionAs':
			result = <span> One or more of your benefits questions has not been answered.
			 Providing this1 information could increase your overall score.</span>;
		break;
		case 'percentTmpWorker':
			result = <span> The % of temp workers is {values[0]}%.</span>;
		break;
	}

	//console.log(result +"abc");

	return result;
}

export {
	ComponentHelper
}
