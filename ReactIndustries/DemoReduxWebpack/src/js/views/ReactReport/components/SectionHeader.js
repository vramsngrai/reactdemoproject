import React from 'react'
import PropTypes from 'prop-types';


class SectionHeader extends React.Component{

	constructor(props){
		super(props);	
		this.setupStyle();
	}

	setupStyle(){
		this.style = {
  			color: 'white',
  			backgroundColor: 'rgb(27, 94, 164)',
		};
		this.headingStyle = {
			color: 'white',
			fontSize: (this.props.fontSize !== undefined)? this.props.fontSize : 24,
			margin: 0,
			padding: 8,
			paddingLeft: 10,
			paddingBottom: '10px',
			textAlign: 'left'

		};
	}	

	render(){
		return (
				<div style={this.style} className = "SessionHeader">
					<h1 style={this.headingStyle}>{this.props.title}</h1>
				</div>
			);
	}

}

SectionHeader.propTypes = {
	fontSize: PropTypes.number
}

export default SectionHeader;