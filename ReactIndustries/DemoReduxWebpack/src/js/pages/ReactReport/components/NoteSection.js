
import React from 'react'
import PropTypes from 'prop-types';
import SectionHeader from './SectionHeader';
import {ComponentHelper} from './ComponentHelper';

//import { Rating } from 'material-ui-rating'

/*
const RATING_PATTERN_PREFIX = '%$rating';//'/$rating/i/\d$/';
const RATING_PATTERN_POSTFIX = '$%';//'/$rating/i/\d$/';*/

class NoteSection extends React.Component{
	constructor(props){
		super(props);
	}

	render(){
		return (
			<div>
				<SectionHeader title="NOTES" fontSize={20} />
				<NoteSectionBody rowData = {this.props.model} />
			</div>
			);
	}
}


class NoteSectionBody extends React.Component{
	render(){
		return (
			<nav style = {{textAlign: 'left', paddingTop: '12px'}} className='textStyleBody2'>
				<ul>
					{this.props.rowData.map((row, index) => (
							<li key={index}>
								{ComponentHelper.getNoteValue(row.type, row.value)}
							</li>
						))}

				</ul>
			</nav>


		)
	}


}

NoteSection.propTypes = {
	model: PropTypes.array
}

NoteSectionBody.propTypes = {
	rowData: PropTypes.array
}

export default NoteSection;