import React from 'react'
//import {MaterialButtonStyles} from './AppStyles'
import PropTypes from 'prop-types';

import SectionHeader from './SectionHeader';
//import StarRating from 'react-bootstrap-star-rating';
/*import { Rating } from 'material-ui-rating'*/
var Rating = require('react-rating');


class ReportHeaderSection extends React.Component{
	constructor(props){
		super(props);
		this.headingStyle = {
			color: 'white',
			margin: 0,
			paddingRight: 5,
			paddingTop: 20,
			paddingBottom: 20,
			paddingLeft: 20,
			textAlign: 'left'

		};
	}

	render(){
		return (
			<div>
				<SectionHeader title="Report" fontSize={28} />
				<div id="ReportHeaderSection-Body">
					<div style={this.headingStyle} id="ReportHeaderSection-model">
						<h2 style={{color: '#000', margin:0}}>{this.props.model.name}</h2>
						<p style={{color: '#000', fontSize: 30, margin:0}} >{this.props.model.quarter}</p>
					</div>
					<div id="ReportHeaderSection-rating">
						<Rating
						  placeholderRate={this.props.model.rating}
						  empty={<img alt='icon' src="src/assets/images/star-empty.png" className="icon ReportHeaderSection-RatingIcon" />}
						  placeholder={<img alt='icon' src="src/assets/images/star-full.png" className="icon ReportHeaderSection-RatingIcon" />}
						  full={<img alt='icon' src="src/assets/images/star-full.png" className="icon ReportHeaderSection-RatingIcon" />}
						  readonly
						/>

					</div>
				</div>
				<div className="clear"/>
			</div>
		);

	}
}

ReportHeaderSection.propTypes = {
	model: PropTypes.object
}

export default ReportHeaderSection;
