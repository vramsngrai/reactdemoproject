
import React from 'react'
import Select from 'react-select';
import 'react-select/dist/react-select.css';


/*var options = [
  { value: 'one', label: 'One' },
  { value: 'two', label: 'Two' }
];*/

function buildSelectOptions(len){
	var result = [];
	for(let i = 0; i< len; i++){
		result.push({value: i, label: 'Model ' + i});
	}
	return result;
}

class HeaderSection extends React.Component{
/*	handleChange(val) {
	  console.log("Selected: " + JSON.stringify(val));
	  this.onSelectModelChange(val);
	}
*/
	render(){
		var options = buildSelectOptions(this.props.modelLen);
		return <header
				>
				<div className="HeaderSection-select content">
					<Select
					  name="form-field-name"
					  options={options}
					  value={this.props.currentModelIndex}
					  onChange={this.props.onSelectModelChange}
					/>
				</div>
				</header>;
	}

}

export default HeaderSection;