
const webpack = require('webpack');
const path = require('path');
const DashboardPlugin = require('webpack-dashboard/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');


const jsSourcePath = path.join(__dirname, './src/js');
const buildPath = path.join(__dirname, './build');
const imgPath = path.join(__dirname, './src/assets/img');
const sourcePath = path.join(__dirname, './src');
const PUBLIC_PATH = path.join(__dirname, './src/assets');

const nodeEnv = process.env.NODE_ENV || 'development';

// Common plugins
const plugins = [
new webpack.optimize.CommonsChunkPlugin({
	name: 'vendor',
	minChunks: Infinity,
	filename: ('vendor-[hash].js'),
}),
new webpack.DefinePlugin({
	'process.env': {
		NODE_ENV: JSON.stringify(nodeEnv),
	},
}),
new webpack.NamedModulesPlugin(),
new HtmlWebpackPlugin({
	template: path.join(sourcePath, 'index.html'),
	path: buildPath,
	filename: ('index.html'),
}),
new webpack.LoaderOptionsPlugin({
	options: {
		postcss: [
			autoprefixer({
				browsers: [
					'last 3 version',
					'ie >= 10',
				],
			}),
		],
		context: sourcePath,
	},
}),
];
/*// Development plugins this cause runing error
plugins.push(
	new webpack.HotModuleReplacementPlugin(),
	new DashboardPlugin()
);*/


// Common rules
const rules = [
  {
    test: /\.(js|jsx)$/,
    exclude: /node_modules/,
    use: [
      'babel-loader',
    ],
  },
  {
    test: /\.(png|gif|jpg|svg)$/,
    include: imgPath,
    use: 'url-loader?limit=20480&name=assets/[name]-[hash].[ext]',
  },
];

// Development rules
rules.push(
	{
		test: /\.scss$/,
		exclude: /node_modules/,
		use: [
			'style-loader',
			// Using source maps breaks urls in the CSS loader
			// https://github.com/webpack/css-loader/issues/232
			// This comment solves it, but breaks testing from a local network
			// https://github.com/webpack/css-loader/issues/232#issuecomment-240449998
			// 'css-loader?sourceMap',
			'css-loader',
			'postcss-loader',
			'sass-loader?sourceMap',
		],
	}
);

rules.push(
	{
			test: /\.css$/,
			use: [{
					loader: "style-loader" // creates style nodes from JS strings
			}, {
					loader: "css-loader" // translates CSS into CommonJS
			}]
	}
);

rules.push(
	{
					test: /\.scss$/,
					use: [{
							loader: "style-loader" // creates style nodes from JS strings
					}, {
							loader: "css-loader" // translates CSS into CommonJS
					}, {
							loader: "sass-loader" // compiles Sass to CSS
					}]
			}
);


//------------------------------------------------exports-------------------------

module.exports = {
	context: jsSourcePath,
	entry: "./index.js",
	output: {
		filename: "./build/bundle.js"
	},
	devServer: {
		inline: true,
		port: 2020
	},
	plugins,
	module: {
		rules,
	},
	resolve: {
    extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js', '.jsx'],
    modules: [
      path.resolve(__dirname, 'node_modules'),
      jsSourcePath,
			PUBLIC_PATH,
    ],
  },
	stats: {
    colors: true,
    modules: true,
    reasons: true,
    errorDetails: true
	}
}
