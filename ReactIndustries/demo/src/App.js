import React, { Component } from 'react';
import 'react-select/dist/react-select.css';

//import logo from './logo.svg';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ReportHeaderSection from './components/ReportHeaderSection';
import ScoringDetailSection from './components/ScoringDetailSection';
import NoteSection from './components/NoteSection';
import HeaderSection from './components/HeaderSection';
var models = require('./model.json').data 


class App extends Component {

  constructor(props){
    super(props);
    this.state = {}
    this.state.modelId = 0;
    this.state.model = models[0];
    this.onSelectModelChange = this.onSelectModelChange.bind(this);
    this.refreshModel = this.refreshModel.bind(this);
    
    this.refreshModel();
  }

  refreshModel(){
    this.setState({
        model: models[this.state.modelId]
    });
  }

  onSelectModelChange(val){
    console.log('onSelectModelChange=' + JSON.stringify(val));
    this.setState({modelId: val.value});
    this.refreshModel();
  }


  render() {
    return (
      <MuiThemeProvider >
        <div className="App">
              <HeaderSection currentModelIndex = {this.state.modelId} modelLen = {models.length} onSelectModelChange={this.onSelectModelChange}/>
              <ReportHeaderSection model={this.state.model.info}/>            
              <ScoringDetailSection model={this.state.model.scoring_details}/>
              <NoteSection model={this.state.model.notes}/>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
