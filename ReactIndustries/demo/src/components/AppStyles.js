
const MaterialButtonStyles = {
	item:{
		small:{
			width: 20,
			height: 20,
			padding: 0
		},
		large:{
			width: 56,
			height: 56,
			padding: 0
		}
	},
	icon:{
		small:{
			width: 20,
			height: 20,
			padding: 0
		},
		large:{
			width: 56,
			height: 56,
			padding: 0
		}
	},
	color:{

	}
}

export {
	MaterialButtonStyles
}