
module.exports = {
	entry: "./src/index.js",

	output: {
		filename: "./build/bundle.js"
	},

	devServer: {
		inline: true,
		port: 2020
	},

	module: {
		rules: [
			{
				test: /\.jsx?$/,
			    loader: 'babel-loader',
			    exclude: /node_modules/,
			    query: {
			        presets: ['es2015']
			    }
			},{
	            test: /\.scss$/,
	            use: [{
	                loader: "style-loader" // creates style nodes from JS strings 
	            }, {
	                loader: "css-loader" // translates CSS into CommonJS 
	            }, {
	                loader: "sass-loader" // compiles Sass to CSS 
	            }]
	        },{
	            test: /\.css$/,
	            use: [{
	                loader: "style-loader" // creates style nodes from JS strings 
	            }, {
	                loader: "css-loader" // translates CSS into CommonJS 
	            }]
	        }

	        ]
		},
	stats: {
            colors: true,
            modules: true,
            reasons: true,
            errorDetails: true
	}
}
