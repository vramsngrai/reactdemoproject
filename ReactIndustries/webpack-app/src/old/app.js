
import React from "react"
import ReactDOM from "react-dom"
//import {Router, browserHistory} from "react-router"

import {
  BrowserRouter as Router,
  Switch,
  Route,
  browserHistory,
  Link,
  IndexRoute
} from 'react-router-dom'


import routes from "./routes.js"

/*const App = () =>{
	return (
			<h1> Hello from React Webpack </h1>
		);
}
*/
ReactDOM.render(
    <Router history={browserHistory} routes={routes} />,
    document.getElementById( "app" )
)
