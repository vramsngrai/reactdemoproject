
i//import {Router, IndexRoute, Route, browserHistory} from "react-router";

import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  IndexRoute
} from 'react-router-dom'


import Layout  from "./layout.react";
import HomePage  from "./pages/homePage.react";
import AboutPage  from "./pages/aboutPage.react";
import ContactPage  from "./pages/contactPage.react";

const routes = (
	<Switch  exact path="/" component={Layout}>
		<IndexRoute component={HomePage} />
		<Route path="about" component={AboutPage}/>
		<Route path="contact" component={ContactPage} />
	</Switch>	
);

export default routes;