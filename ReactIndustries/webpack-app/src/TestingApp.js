import React, { Component } from 'react';
import ReactDOM from "react-dom"

import 'react-select/dist/react-select.css';
//import 'bootstrap/dist/css/bootstrap.css';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  browserHistory,
  Link,
  IndexRoute
} from 'react-router-dom'

//import logo from './logo.svg';
import './TestingApp.css';
import ReportHeaderSection from './components/ReportHeaderSection';
import ScoringDetailSection from './components/ScoringDetailSection';
import NoteSection from './components/NoteSection';
import HeaderSection from './components/HeaderSection';
import {AuthPage} from "./pages/authPages.react";

var models = require('./models/model.json').data 


class TestingApp extends Component {

  constructor(props){
    super(props);
    this.state = {}
    this.state.modelId = 0;
    this.state.model = models[0];
    this.onSelectModelChange = this.onSelectModelChange.bind(this);
    this.refreshModel = this.refreshModel.bind(this);
    
    this.refreshModel();
  }

  refreshModel(){
    this.setState({
        model: models[this.state.modelId]
    });
  }

  onSelectModelChange(val){
    console.log('onSelectModelChange=' + JSON.stringify(val));
    this.setState({modelId: val.value});
    this.refreshModel();
  }


  render() {
    return (
    	<Router>
	      <div className="App">
	      		<AuthPage> 
	            <HeaderSection currentModelIndex = {this.state.modelId} modelLen = {models.length} onSelectModelChange={this.onSelectModelChange}/>
	            <ReportHeaderSection model={this.state.model.info}/>            
	            <ScoringDetailSection model={this.state.model.scoring_details}/>
	            <NoteSection model={this.state.model.notes}/>
            </AuthPage>
	      </div>
     	</Router>
    );
  }
}

export default TestingApp;