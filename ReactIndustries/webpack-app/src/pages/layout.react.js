import React from 'react'
import { Link } from "react-router-dom";

class Layout extends React.Component{


	render(){
		return (
			<div style={{width:"100%", height:"100%", backgroundColor: "red"}}>
				<div className={"App-header-nav"}>
					<ul>
						<li><Link to="/">Home</Link></li>
						<li><Link to="/about"> About </Link></li>
						<li><Link to="/contact">Contact</Link></li>
					</ul>
				</div>
				<div> {this.props.children} </div>
			</div>
		);
	}

};

export default Layout;