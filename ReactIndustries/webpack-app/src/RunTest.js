
import React from "react"
import ReactDOM from "react-dom"
import TestingApp from "./TestingApp";


ReactDOM.render(
		<TestingApp />,
		document.getElementById("app")
);