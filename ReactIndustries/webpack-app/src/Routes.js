
import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  IndexRoute
} from 'react-router-dom'


import HomePage  from "./pages/homePage.react";
import AboutPage  from "./pages/aboutPage.react";
import ContactPage  from "./pages/contactPage.react";

const AppSwitch = () =>{
	<Switch>
		<div>
			<Route path="/" component={HomePage} />
			<Route path="about" component={AboutPage}/>
			<Route path="contact" component={ContactPage} />
		</div>	
   </Switch>
};

export default AppSwitch;