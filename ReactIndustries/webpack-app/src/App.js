
import React from "react"
//import {Router, browserHistory} from "react-router"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  browserHistory,
  Link,
  IndexRoute
} from 'react-router-dom'

import Layout  from "./pages/layout.react";
import AboutPage from "./pages/aboutPage.react"
import HomePage from "./pages/homePage.react"

import {AppAuth} from "./pages/AppAuth"


class App extends React.Component{

	render(){
		return (
			<AppAuth />
		);
	}
};

export default App;